INSERT INTO author (id, name) VALUES (1, 'Stephen King');
INSERT INTO book (author_id, isbn, title) VALUES (1, 1234567890123, 'Book of secrets');
INSERT INTO book_comments (book_isbn, comments) VALUES (1234567890123, 'Comment #1');
