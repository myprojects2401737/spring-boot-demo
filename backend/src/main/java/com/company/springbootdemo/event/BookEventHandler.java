package com.company.springbootdemo.event;

import com.company.springbootdemo.persistence.entity.Book;
import org.springframework.data.rest.core.annotation.HandleAfterCreate;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
@RepositoryEventHandler
public class BookEventHandler {
    private final Logger logger = Logger.getLogger("Class BookEventHandler");

    @HandleAfterCreate
    public void beforeCreate(Book book) {
        logger.info(String.format("New book created: %s", book));
    }
}
