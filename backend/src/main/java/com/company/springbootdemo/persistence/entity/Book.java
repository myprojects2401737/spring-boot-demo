package com.company.springbootdemo.persistence.entity;

import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.Data;

import java.util.List;

import static jakarta.persistence.FetchType.EAGER;

@Entity
@Data
public class Book {
    @Id
    @Max(value = 9_9999_9999_9999L, message = "{isbn.size}")
    private Long isbn;

    @ManyToOne(optional = false)
    private Author author;

    @NotBlank(message = "{title.mandatory}")
    @Size(min = 1, max = 64, message = "title.size")
    private String title;

    @ElementCollection(targetClass = String.class, fetch = EAGER)
    private List<String> comments;
}
