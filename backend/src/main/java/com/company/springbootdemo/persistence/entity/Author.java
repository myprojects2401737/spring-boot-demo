package com.company.springbootdemo.persistence.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.Data;

import java.util.Set;

import static jakarta.persistence.FetchType.EAGER;

@Entity
@Data
public class Author {
    @Id
    private Long id;

    @NotBlank(message = "{author.name.mandatory}")
    @Size(min = 5, max = 64, message = "{author.size}")
    private String name;

    @OneToMany(mappedBy = "author", fetch = EAGER)
    private Set<Book> books;
}
