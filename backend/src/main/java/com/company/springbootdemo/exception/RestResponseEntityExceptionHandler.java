package com.company.springbootdemo.exception;

import com.fasterxml.jackson.core.JsonParseException;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.sql.SQLException;
import java.util.Date;

import static java.util.stream.Collectors.joining;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

@ControllerAdvice
public class RestResponseEntityExceptionHandler {

    private static final String SQL_ERROR = "SQL error!";
    private static final String PARSING_ERROR = "Parsing error!";
    private static final String VALIDATION_ERROR = "Validation error!";

    @ExceptionHandler(value = {SQLException.class})
    private ResponseEntity<ErrorMessage> handle(SQLException ex) {
        return new ResponseEntity<>(
                new ErrorMessage(
                        BAD_REQUEST.value(),
                        new Date(),
                        SQL_ERROR,
                        ex.getMessage()),
                BAD_REQUEST);
    }

    @ExceptionHandler(value = {JsonParseException.class})
    private ResponseEntity<ErrorMessage> handle(JsonParseException ex) {
        return new ResponseEntity<>(
                new ErrorMessage(
                        BAD_REQUEST.value(),
                        new Date(),
                        PARSING_ERROR,
                        ex.getMessage()),
                BAD_REQUEST);
    }

    @ExceptionHandler(value = {ConstraintViolationException.class})
    private ResponseEntity<ErrorMessage> handle(ConstraintViolationException ex) {
        return new ResponseEntity<>(
                new ErrorMessage(
                        BAD_REQUEST.value(),
                        new Date(),
                        VALIDATION_ERROR,
                        ex.getConstraintViolations().stream().map(ConstraintViolation::getMessage).collect(joining())),
                BAD_REQUEST);
    }

    @ExceptionHandler(value = {MethodArgumentNotValidException.class})
    private ResponseEntity<ErrorMessage> handle(MethodArgumentNotValidException ex) {
        return new ResponseEntity<>(
                new ErrorMessage(
                        BAD_REQUEST.value(),
                        new Date(),
                        VALIDATION_ERROR,
                        ex.getMessage()),
                BAD_REQUEST);
    }
}
