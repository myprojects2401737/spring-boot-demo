package com.company.springbootdemo.mapper;

import com.company.springbootdemo.model.Book;
import org.mapstruct.Mapper;

@Mapper
public interface BookMapper {
    com.company.springbootdemo.persistence.entity.Book toEntity(Book source);
    Book toPojo(com.company.springbootdemo.persistence.entity.Book destination);
}
