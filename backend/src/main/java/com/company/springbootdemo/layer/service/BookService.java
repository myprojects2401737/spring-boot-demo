package com.company.springbootdemo.layer.service;

import com.company.springbootdemo.model.Book;

public interface BookService {
    Book findByIsbn(Long isbn);
}
