package com.company.springbootdemo.layer.controller;

import com.company.springbootdemo.layer.service.BookService;
import com.company.springbootdemo.model.Book;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import static org.springframework.http.ResponseEntity.notFound;
import static org.springframework.http.ResponseEntity.ok;

@RepositoryRestController("book")
public class BookController {

    private final BookService bookService;

    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping("isbn/{isbn}")
    public ResponseEntity<Book> getBookByIsbn(@PathVariable("isbn") Long isbn) {
        Book book = bookService.findByIsbn(isbn);
        return book != null
                ? ok(book)
                : notFound().build();
    }
}
