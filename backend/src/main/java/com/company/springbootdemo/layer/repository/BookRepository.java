package com.company.springbootdemo.layer.repository;

import com.company.springbootdemo.persistence.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.List;

@RepositoryRestResource(collectionResourceRel = "book", path = "book")
public interface BookRepository extends JpaRepository<Book, Long> {

    // path: root/book/search/findByAuthorName?authorName=DrSomeOne
    @RestResource
    List<Book> findByAuthorName(@Param("authorName") String authorName);

    @Override
    @RestResource(exported = false)
    void deleteById(Long aLong);

//    @Query
}
