package com.company.springbootdemo.layer.repository;

import com.company.springbootdemo.persistence.entity.Author;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "author", path = "author")
public interface AuthorRepository extends JpaRepository<Author, Long> {
}
