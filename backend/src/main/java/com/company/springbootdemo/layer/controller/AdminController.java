package com.company.springbootdemo.layer.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("admin")
public class AdminController {

    @GetMapping
    public String getAdmin() {
        return "You're an admin. Good for you!";
    }
}
