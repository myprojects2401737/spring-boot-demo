package com.company.springbootdemo.layer.service;

import com.company.springbootdemo.layer.repository.BookRepository;
import com.company.springbootdemo.mapper.BookMapper;
import com.company.springbootdemo.model.Book;
import org.springframework.stereotype.Service;

@Service
public class BookServiceImpl implements BookService {

    private final BookRepository bookRepository;
    private final BookMapper bookMapper;

    public BookServiceImpl(BookRepository bookRepository, BookMapper bookMapper) {
        this.bookRepository = bookRepository;
        this.bookMapper = bookMapper;
    }

    @Override
    public Book findByIsbn(Long isbn) {
        return bookMapper.toPojo(bookRepository.findById(isbn).get());
    }
}
