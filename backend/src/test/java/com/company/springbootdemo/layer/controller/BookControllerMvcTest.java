package com.company.springbootdemo.layer.controller;

import com.company.springbootdemo.layer.service.BookService;
import com.company.springbootdemo.model.Author;
import com.company.springbootdemo.model.Book;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class BookControllerMvcTest {

    private static final long ISBN = 1_1234_1234_1234L;
    private static final String AUTHOR = "AUTHOR";
    private static final String TITLE = "TITLE";
    private static final String ADMIN_ROLE = "ADMIN";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private BookService bookService;

    @Mock
    private Book book;

    @Test
    @WithAnonymousUser
    void shouldReturn401WhenUserIsUnauthorized() throws Exception {
        mockMvc.perform(get("/admin")).andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(roles = {})
    void shouldReturn403WhenUserIsAuthorizedButHasNoAdminRole() throws Exception {
        mockMvc.perform(get("/admin")).andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(roles = ADMIN_ROLE)
    void shouldReturn200WhenUserIsAuthorizedAndHasAdminRole() throws Exception {
        mockMvc.perform(get("/admin")).andExpect(status().isOk());
    }

    @Test
    @WithAnonymousUser
    void shouldReturn404WhenServiceReturnsNull() throws Exception {
        when(bookService.findByIsbn(ISBN)).thenReturn(null);
        mockMvc.perform(get("/book/isbn/{isbn}", ISBN)).andExpect(status().isNotFound());
    }

    @Test
    @WithAnonymousUser
    void shouldReturn200WhenServiceReturnsEntity() throws Exception {
        when(bookService.findByIsbn(ISBN)).thenReturn(book);
        mockMvc.perform(get("/book/isbn/{isbn}", ISBN)).andExpect(status().isOk());
    }

    @Test
    @WithAnonymousUser
    void shouldReturn201() throws Exception {
        mockMvc.perform(post("/author")
                        .content(asJsonString(new Author(2L, AUTHOR, null)))
                        .contentType(APPLICATION_JSON)
                        .accept(APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    private static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
