package com.company.springbootdemo.layer.controller;

import com.company.springbootdemo.layer.service.BookService;
import com.company.springbootdemo.model.Book;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class BookControllerTest {

    private static final long ISBN = 1234_1234_1234L;

    @Mock
    private BookService bookService;

    @InjectMocks
    private BookController underTest;

    @Mock
    private Book expected;

    @Test
    void getBookByIsbnShouldReturnServiceResponseWhenNotNull() {
        when(bookService.findByIsbn(ISBN)).thenReturn(expected);

        ResponseEntity<Book> actual = underTest.getBookByIsbn(ISBN);

        verify(bookService, times(1)).findByIsbn(ISBN);
        assertThat("Same instance should be returned.", actual.getBody(), sameInstance(expected));
    }
}
